﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyFirstWebApp.Models;
using System.Web;
using System.Text;
using MyFirstWebApp.Views.Home;

namespace MyFirstWebApp.Controllers
{
    public class HomeController : Controller
    {
        IWebHostEnvironment _appEnvironment;
        FileModel file;

        public HomeController( IWebHostEnvironment appEnvironment, FileModel file)
        {
            _appEnvironment = appEnvironment;
            this.file = file;
        }

        public IActionResult StartMenu()
        {
            return View("StartMenu");
        }
        public IActionResult Index()
        {
            return View("Index");
        }
        public IActionResult TextFromScreen()
        {
            return View("TextFromScreen");
        }
        public IActionResult ChipherChoise()
        {
            return View("ChipherChoise");
        }
        [HttpPost]
        public IActionResult AddFileandKey(IFormFile uploadedFile, string key)
        {
            file.AddFileAndKey(uploadedFile, key, _appEnvironment);
            if (file.Exception != "")
            {
                return View("Index", file);
            }
            if (FileModel.ID==0)
            {
                file.DecryptionTxt();
            }
            else 
            {
                file.CipherText();
            }
            return View("AfterDownload", file);
        }
        public IActionResult AddText(string text, string key)
        {
            file.Exception = "";
            if (file.AddText(text, key))
            {
                file.CipherText();
                return View("AfterDownload", file);
            }
            return View("TextFromScreen", file);
        }
        public IActionResult SaveFile(string directory, string name, string format)
        {
            file.Exception = "";
            try
            {
                file.SaveFile(directory, name , format);
            }
            catch(Exception e)
            {
                file.Exception = "Введено некорректное значение";
            }
            return View("AfterDownload", file);
        }
        public IActionResult GetFile(string name, string format)
        {

            return File( file.GetFile(_appEnvironment, format), "application/xml", name+format);
        }

    }
}