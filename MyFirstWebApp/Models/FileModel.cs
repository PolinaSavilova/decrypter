﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.Web;
using Word = Microsoft.Office.Interop.Word;


namespace MyFirstWebApp.Models
{
    public class FileModel
    {
        public string Key { get; set; }
        public string Text { get; set; }
        public static int ID { get; set; }
        public string Exception { get; set; }
        public string Rep { get; set; }
        public FileModel()
        {
            Rep = "\\Files\\Text";
            Text = "";
        }

        public void CipherText()
        {
            string rusAlf = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            string cipherText = "";
            for (int i = 0; i < Text.Length;)
            {
                for (int j = 0; j < Key.Length;)
                {
                    if (rusAlf.Contains(Text[i]))
                    {
                        int numberEncryptionletter = rusAlf.IndexOf(Text[i]) + rusAlf.IndexOf(Key[j]);
                        if (numberEncryptionletter >= 33)
                        {
                            numberEncryptionletter = (rusAlf.IndexOf(Text[i]) + rusAlf.IndexOf(Key[j])) - 33;
                        }
                        cipherText = cipherText + rusAlf[numberEncryptionletter];
                        j++;
                    }
                    else
                    {
                        cipherText = cipherText + Text[i];
                    }
                    i++;
                    if (i >= Text.Length)
                    {
                        break;
                    }
                }
            }
            Text = cipherText;
        }
        public void DecryptionTxt()
        {
            string rusAlf = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            string decryptionTxt = "";
            for (int i = 0; i < Text.Length;)
            {
                for (int j = 0; j < Key.Length;)
                {
                    if (rusAlf.Contains(Text[i]))
                    {
                        int numberDecryptionletter = rusAlf.IndexOf(Text[i]) - rusAlf.IndexOf(Key[j]);
                        if (numberDecryptionletter < 0)
                        {
                            numberDecryptionletter = (rusAlf.IndexOf(Text[i]) - rusAlf.IndexOf(Key[j])) + 33;
                        }
                        decryptionTxt = decryptionTxt + rusAlf[numberDecryptionletter];
                        j++;
                    }
                    else
                    {
                        decryptionTxt = decryptionTxt + Text[i];
                    }
                    i++;
                    if (i >= Text.Length)
                    {
                        break;
                    }
                }
            }
            Text = decryptionTxt;
        }
        public void SaveFile(string directory, string name, string format)
        {
            Exception = "";
            if (format == ".docx")
            {
                Object filename = directory + "/" + name + format;

                Word.Application Progr = new Word.Application();
                Progr.Documents.Add();
                Word.Document Doc = Progr.Documents.Application.ActiveDocument;
                Word.Range rng = Doc.Range();
                rng.Text = Text;
                try
                { 
                    Doc.SaveAs(ref filename);
                }
                catch
                {
                    this.Exception = "Введено некорректное значение";
                }
                Progr.Documents.Close();
                Progr.Quit();
            }
            else
            {
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                Encoding enc = Encoding.GetEncoding("windows-1251");
                try
                {
                    StreamWriter streamWriter = new StreamWriter(directory + "/" + name + format, false, enc);
                    streamWriter.Write(Text);
                    streamWriter.Close();
                }
                catch
                {
                    this.Exception = "Введено некорректное значение";
                }
            }
        }
        public void AddFileAndKey(IFormFile uploadedFile, string key, IWebHostEnvironment appEnvironment)
        {
            if ((uploadedFile != null) && (key != null))
            {
                // путь к папке Files
                string path = "/Files/" + uploadedFile.FileName;
                string textFromFile;
                // сохраняем файл в папку Files в каталоге wwwroot
                FileStream fileStream = new FileStream(appEnvironment.WebRootPath + path, FileMode.Create);
                uploadedFile.CopyTo(fileStream);
                fileStream.Close();

                if (uploadedFile.FileName.EndsWith(".docx"))
                {
                    Object filename = appEnvironment.WebRootPath + path;
                    Word.Application Progr = new Word.Application();
                    Progr.Documents.Open(ref filename);
                    Word.Document Doc = Progr.Documents.Application.ActiveDocument;
                    Word.Range Rng = Doc.Range();
                    textFromFile = Rng.Text;
                    Progr.Documents.Close();
                    Progr.Quit();
                }
                else
                {
                    Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                    Encoding enc = Encoding.GetEncoding("windows-1251");
                    StreamReader sr = new StreamReader(appEnvironment.WebRootPath + path, enc);
                    textFromFile = sr.ReadToEnd();
                    sr.Close();
                }
                this.AddText(textFromFile, key);
                File.Delete(appEnvironment.WebRootPath + path);
            }
            else 
            {
                this.Exception = "Файл не выбран или не введен ключ";
            }
        }
        public string GetFile(IWebHostEnvironment appEnvironment, string format)
        {
            if (format == ".docx")
            {
                Object filename = appEnvironment.WebRootPath + "\\Files\\Text.docx";
                Word.Application Progr = new Word.Application();
                Progr.Documents.Add();
                Word.Document Doc = Progr.Documents.Application.ActiveDocument;
                Word.Range rng = Doc.Range();
                rng.Text = Text;
                Doc.SaveAs(ref filename);
                Progr.Documents.Close();
                Progr.Quit();
                return Rep+format;
            }
            else
            {
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                Encoding enc = Encoding.GetEncoding("windows-1251");
                StreamWriter streamWriter = new StreamWriter(appEnvironment.WebRootPath + Rep + format, false, enc);
                streamWriter.Write(Text);
                streamWriter.Close();
                return Rep+format;
            }
        }
        public bool AddText(string text, string key)
        {
            Exception = "";
            if ((text != null))
            {
                this.Text = text;
            }
            else
            { 
                Exception = "Введите текст";
                return false;
            }

            string rusAlf = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            if ((key == null)||(key==""))
            {
                Exception = "Введите ключ";
                return false;
            }
            
            key = key.ToLower();
            foreach(char i in key)
            {
                if (!rusAlf.Contains(i))
                {
                    Exception = "Был введен неправильный ключ, ключ должен содержать только символы кирилицы.\n Повторите ввод";
                    return false;
                }
            }
            this.Key = key;
            return true;
        }
    }
}
