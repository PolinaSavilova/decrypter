﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MyFirstWebApp.Controllers;
using MyFirstWebApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MyFirstWebApp.Controllers.Tests
{
    [TestClass()]
    public class HomeControllerTests
    {
        [TestMethod()]
        public void StartMenu_ViewResultNotNull_AndLeadsIntoTheCorrectView()
        {
            // arrange
            string expected = "StartMenu";
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            var mockFile = new Mock<FileModel>();
            var controller = new HomeController(mockEnvironment.Object, mockFile.Object);

            // act
            ViewResult result = controller.StartMenu() as ViewResult;

            // assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ViewName);

        }

        [TestMethod()]
        public void IndexView_ResultNotNull_AndLeadsIntoTheCorrectView()
        {
            // arrange
            string expected = "Index";
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            var mockFile = new Mock<FileModel>();
            var controller = new HomeController(mockEnvironment.Object, mockFile.Object);

            // act
            ViewResult result = controller.Index() as ViewResult;

            // assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ViewName);

        }

        [TestMethod]
        public void TextFromScreen_ViewResultNotNull_AndLeadsIntoTheCorrectView()
        {
            // arrange
            string expected = "TextFromScreen";
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            var mockFile = new Mock<FileModel>();
            var controller = new HomeController(mockEnvironment.Object, mockFile.Object);

            // act
            ViewResult result = controller.TextFromScreen() as ViewResult;

            // assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ViewName);

        }

        [TestMethod]
        public void ChipherChoise_ViewResultNotNull_AndLeadsIntoTheCorrectView()
        {
            // arrange
            string expected = "ChipherChoise";
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            var mockFile = new Mock<FileModel>();
            var controller = new HomeController(mockEnvironment.Object, mockFile.Object);

            // act
            ViewResult result = controller.ChipherChoise() as ViewResult;

            // assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result.ViewName);

        }

        [TestMethod()]
        public void SaveFileTestRightUserEnter_ViewResultNotNull_AndLeadsIntoTheCorrectView()
        {
            // arrange
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            var mockFile = new FileModel();
            var controller = new HomeController(mockEnvironment.Object, mockFile);
            string directory = "C:\\Users\\Public\\Downloads";
            string name = "name";
            string format = ".docx";

            // act
            ViewResult result = controller.SaveFile(directory, name, format) as ViewResult;

            // assert
            Assert.IsNotNull(result);
            Assert.AreEqual("", mockFile.Exception, mockFile.Exception);
            Assert.AreEqual("AfterDownload", result.ViewName);

            //arrange
            format = ".txt";

            // act
            result = controller.SaveFile(directory, name, format) as ViewResult;

            // assert
            Assert.IsNotNull(result);
            Assert.AreEqual("", mockFile.Exception);
            Assert.AreEqual("AfterDownload", result.ViewName);
        }

        [TestMethod()]
        public void SaveFileTestWrongUserEnter_ViewResultNotNull_AndShowMessage()
        {
            // arrange
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            var mockFile = new Mock<FileModel>();
            var controller = new HomeController(mockEnvironment.Object, mockFile.Object);
            string directory = "asdfghjkl;";
            string name = "Nana";
            string format = ".txt";

            // act
            ViewResult result = controller.SaveFile(directory, name, format) as ViewResult;

            // assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Введено некорректное значение", mockFile.Object.Exception);
            Assert.AreEqual("AfterDownload", result.ViewName);
        }

        [TestMethod()]
        public void AddTextTestWrongUserEnter_ViewResultNotNull()
        {
            // arrange
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            var mockFile = new Mock<FileModel>();
            var controller = new HomeController(mockEnvironment.Object, mockFile.Object);
            string text = null;
            string key = null;

            // act
            ViewResult result = controller.AddText(text, key) as ViewResult;

            // assert
            Assert.IsNotNull(result);
            Assert.AreEqual("TextFromScreen", result.ViewName);

        }

        [TestMethod()]
        public void AddTextTestRightUserEnter_ViewResultNotNull()
        {
            // arrange
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            var mockFile = new Mock<FileModel>();
            var controller = new HomeController(mockEnvironment.Object, mockFile.Object);
            string text = "паепр";
            string key = "рпрп";

            // act
            ViewResult result = controller.AddText(text, key) as ViewResult;

            // assert
            Assert.IsNotNull(result);
            Assert.AreEqual("AfterDownload", result.ViewName);

        }

        [TestMethod()]
        public void AddFileAndKeyTestWrongUserEnter_ViewResultNotNull()
        {
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            var mockFile = new Mock<FileModel>();
            var controller = new HomeController(mockEnvironment.Object, mockFile.Object);

            var mockUploadedFile = new Mock<IFormFile>();
            var fileName = "Christian.docx";
            string key = null;


            mockUploadedFile.Setup(_ => _.FileName).Returns(fileName);
            mockEnvironment.Setup(_ => _.WebRootPath).Returns("C:\\Users\\Public\\Downloads");

            ViewResult result = controller.AddFileandKey(mockUploadedFile.Object, key) as ViewResult;
            // assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.ViewName);

        }


        [TestMethod()]
        public void AddFileAndKeyTestRightUserEnter_ViewResultNotNull()
        {
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            var mockFile = new Mock<FileModel>();
            var controller = new HomeController(mockEnvironment.Object, mockFile.Object);
            var mockUploadedFile = new Mock<IFormFile>();
            var fileName = "Christian.docx";
            string key = "Чтото";


            mockUploadedFile.Setup(_ => _.FileName).Returns(fileName);
            mockEnvironment.Setup(_ => _.WebRootPath).Returns("C:\\Users\\Public\\Downloads");

            ViewResult result = controller.AddFileandKey(mockUploadedFile.Object, key) as ViewResult;
            // assert
            Assert.IsNotNull(result);
            Assert.AreEqual("AfterDownload", result.ViewName);

        }


        //public void GetFileTest_ViewResultNotNull_DownloadedFileExists()
        //{
        //    // arrange
        //    var mockEnvironment = new Mock<IWebHostEnvironment>();
        //    var mockFile = new Mock<FileModel>();
        //    var controller = new HomeController(mockEnvironment.Object, mockFile.Object);
        //    string name = "Nana";
        //    string format = ".txt";
        //    mockFile.Object.AddText("TExt", "Key");
        //    // act
        //    ViewResult result = controller.GetFile(name, format) as ViewResult;

        //    // assert
        //    Assert.IsNotNull(result);
        //    Assert.AreEqual("\\Files\\Text" + format, mockFile.Object.GetFile(mockEnvironment.Object, format));
        //    Assert.IsFalse(File.Exists(mockFile.Object.Rep+ format));
        //}


    }
}