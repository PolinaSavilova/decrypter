﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MyFirstWebApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MyFirstWebApp.Models.Tests
{
    [TestClass()]
    public class FileModelTests
    {
        [TestMethod()]
        public void AddTextTest()
        {
            FileModel file = new FileModel();
            string key = "Чтото";
            string text = "Что-то этакое";

            bool check = file.AddText(text, key);
            Assert.AreEqual(true, check);
            Assert.AreEqual(text, file.Text, "не сохраняет текст");
            Assert.AreEqual(key.ToLower(), file.Key, "не сохраняет ключ");
        }

        [TestMethod()]
        public void AddTextTest_wrongkKey()
        {
            FileModel file = new FileModel();
            string key = "Что-то";
            string text = "Что-то этакое";

            bool check = file.AddText(text, key);
            Assert.AreEqual(false, check, "Не правильное возвращаемое значение");
            Assert.AreEqual("Был введен неправильный ключ, ключ должен содержать только символы кирилицы.\n Повторите ввод", file.Exception, "Не правильный Exception");
        }

        [TestMethod()]
        public void AddTextTest_nullKey()
        {
            FileModel file = new FileModel();
            string key = "";
            string text = "Что-то этакое";

            bool check = file.AddText(text, key);
            Assert.AreEqual(false, check, "Не правильное возвращаемое значение");
            Assert.AreEqual("Введите ключ", file.Exception, "Не правильный Exception");
        }

        [TestMethod()]
        public void AddTextTest_nullText()
        {
            FileModel file = new FileModel();
            string key = "Чтото";
            string text = "";

            bool check = file.AddText(text, key);
            Assert.AreEqual(true, check, "Не правильное возвращаемое значение");
            Assert.AreEqual("", file.Exception, "Не правильный Exception");
        }

        [TestMethod()]
        public void SaveFileTest_RightUserEnter()
        {
            FileModel file = new FileModel();
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            string directory = "C:\\Users\\Public\\Downloads";
            string name = "ne";
            string format = ".txt";
            string format2 = ".docx";


            file.Text = "res";
            file.SaveFile(directory, name, format);
            file.SaveFile(directory, name, format2);
            FileStream fs = File.OpenRead(directory + "/" + name + format);
            StreamReader sr = new StreamReader(fs);
            string result = sr.ReadToEnd();
            sr.Close();
            fs.Close();

            Assert.IsTrue(File.Exists(directory + "/" + name + format));
            Assert.AreEqual(file.Text, result, result);
            Assert.AreEqual("", file.Exception, file.Exception);
            Assert.IsTrue(File.Exists(directory + "/" + name + format2));

        }

        [TestMethod()]
        public void SaveFileTest_wrongDirectory()
        {
            FileModel file = new FileModel();
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            string directory = "C:\\UsersPublic\\Downloads";
            string name = "ne";
            string format = ".txt";

            file.SaveFile(directory, name, format);


            Assert.IsFalse(File.Exists(directory + name + format));
            Assert.AreEqual("Введено некорректное значение", file.Exception);
            format = ".docx";

            file.SaveFile(directory, name, format);


            Assert.IsFalse(File.Exists(directory + name + format));
            Assert.AreEqual("Введено некорректное значение", file.Exception);

        }

        [TestMethod()]
        public void CipherTextTest_RightUserEnter()
        {
            FileModel file = new FileModel();
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            file.Text = "яяаар";
            file.Key = "б";

            file.CipherText();

            Assert.AreEqual("ааббс", file.Text);

        }

        [TestMethod()]
        public void DecryptionTxtTest()
        {
            FileModel file = new FileModel();
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            file.Text = "ааббс";
            file.Key = "б";

            file.DecryptionTxt();

            Assert.AreEqual("яяаар", file.Text);

        }

        [TestMethod()]
        public void CipherTextTest_EmptyText()
        {
            FileModel file = new FileModel();
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            file.Text = "";
            file.Key = "б";

            file.CipherText();

            Assert.AreEqual("", file.Text);

        }

        [TestMethod()]
        public void DecryptionTxtTest_EmptyText()
        {
            FileModel file = new FileModel();
            file.Text = "";
            file.Key = "б";

            file.DecryptionTxt();

            Assert.AreEqual("", file.Text);

        }

        [TestMethod()]
        public void AddFileAndKeyTest()
        {
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            var mockFile = new Mock<FileModel>();
            var mockUploadedFile = new Mock<IFormFile>();
            var fileName = "Christian.docx";
            string key = "пвлар";
            mockUploadedFile.Setup(_ => _.FileName).Returns(fileName);
            mockEnvironment.Setup(_ => _.WebRootPath).Returns("C:\\Users\\Public\\Downloads");

            mockFile.Object.AddFileAndKey(mockUploadedFile.Object, key, mockEnvironment.Object);
            Assert.AreEqual("пвлар", mockFile.Object.Key);
        }

        [TestMethod()]
        public void AddFileAndKeyTest_WrongUserEnter()
        {
            string expecting = "Файл не выбран или не введен ключ";
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            var mockFile = new Mock<FileModel>();
            var mockUploadedFile = new Mock<IFormFile>();
            var fileName = "Christian.docx";
            string key = null;
            mockUploadedFile.Setup(_ => _.FileName).Returns(fileName);
            mockEnvironment.Setup(_ => _.WebRootPath).Returns("C:\\Users\\Public\\Downloads");

            mockFile.Object.AddFileAndKey(mockUploadedFile.Object, key, mockEnvironment.Object);

            Assert.AreEqual(expecting, mockFile.Object.Exception);
        }

        [TestMethod()]
        public void GetFileTest_txt()
        {
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            var mockFile = new Mock<FileModel>();
            mockEnvironment.Setup(_ => _.WebRootPath).Returns("C:\\Users\\Public\\Downloads");
            string expected = "\\Files\\Text.txt";

            string result = mockFile.Object.GetFile(mockEnvironment.Object, ".txt");

            Assert.AreEqual(expected, result);
        }
        [TestMethod()]
        public void GetFileTest_docx()
        {
            var mockEnvironment = new Mock<IWebHostEnvironment>();
            var mockFile = new Mock<FileModel>();
            mockEnvironment.Setup(_ => _.WebRootPath).Returns("C:\\Users\\Public\\Downloads");
            string expected = "\\Files\\Text.docx";

            string result = mockFile.Object.GetFile(mockEnvironment.Object, ".docx");

            Assert.AreEqual(expected, result);
        }
    }
}